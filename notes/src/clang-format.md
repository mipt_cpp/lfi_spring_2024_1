`.clang-format`

# Как включить автоматическое красивое форматирование

```cpp
# options: https://clang.llvm.org/docs/ClangFormatStyleOptions.html

BasedOnStyle: LLVM
IndentWidth: 4
# Must be 120 characters or less!
ColumnLimit: 120
# spaces, not tabs!
UseTab: Never
# if (x) doStuff()  is not recommended (but sometimes useful)
AllowShortIfStatementsOnASingleLine: true
# 
AlignTrailingComments: true
SpacesBeforeTrailingComments: 3
#  #define SHORT_NAME       42
#  #define LONGER_NAME      0x007f   # does nice spacing for macros
AlignConsecutiveMacros: Consecutive
# use \n instead of \r\n for win\linux compatibility
UseCRLF: false
AlignConsecutiveAssignments: true

BraceWrapping:
  AfterClass:      false
  AfterControlStatement: false
  AfterEnum:       true
  AfterFunction:   false
  AfterNamespace:  false
  AfterObjCDeclaration: false
  AfterStruct:     true
  AfterUnion:      true
  BeforeCatch:     true
  BeforeElse:      false
  IndentBraces:    false
BreakBeforeBraces: Custom
```

Код для проверки работоспособоности

```cpp
#include <iostream>

#ifndef N
#define N 7
#endif

void readArray(int (&array)[N]) {
    for (unsigned i = 0; i < N; ++i) {std::cin >> array[i];}
}

void printArray(int (&array)[N]) 
{
for(unsigned i=0; i <N;++i){
                std::cout <<array[i]<<" ";
  }
        std::cout << std::endl;
}

int main() {
    int a[N];
    int b=10;
      int    c =12;
  double d= 42;
readArray(a);

        printArray(a);
    return 0;
}
```

Куда положить:

![](https://i.imgur.com/A9A4UEI.png)

Где включить (`File->Preferences->Settings` или сразу `Ctrl+,`):

![](https://i.imgur.com/Gi9ONoz.png)

